<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
    public function product(){
        return $this->hasMany('App\Models\Product', 'product_id', 'id');
    }
    use HasFactory;
}
