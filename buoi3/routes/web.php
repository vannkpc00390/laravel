<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('profile')->with([
        'name' => 'Nguyễn Khánh Văn',
        'year' => 1997 ,
        'school'=> "Cao Đẳng Cần Thơ" ,
        'address' => "Hậu Giang" ,
        'target' => "Đi Thực Tập ở Công Ty"
    ]);
});
Route::get('/cau3', function () {
    return view('index')->with([
        'name' => 'Nguyễn Khánh Văn',
        'year' => 1997 ,
        'school'=> "Cao Đẳng Cần Thơ" ,
        'address' => "Hậu Giang" ,
        'target' => "Đi Thực Tập ở Công Ty"
    ]);
});
Route::get('/cau2', function () {
    return view('list')->with([
        
            
                'name' => 'Học View trong Laravel',
                'status' => 0
            ,
            
                'name' => 'Học Route trong Laravel',
                'status' => 1
            ,
            
                'name' => 'Làm bài tập View trong Laravel',
                'status' => -1
            
       
    ]);
 });