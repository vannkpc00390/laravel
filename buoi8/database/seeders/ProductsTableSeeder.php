<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
            // 10 sản phẩm đầu
            ['name' => 'Iphone 8', 'status' => 0 ],
             ['name' => 'Iphone 6 plus', 'status' => 1 ],
             ['name' => 'Iphone 7', 'status' => 1 ],
             ['name' => 'Iphone 8 plus', 'status' => 1 ],
             ['name' => 'Iphone 7 plus', 'status' => 0 ],
             ['name' => 'Iphone 6','status' => 0 ],
             ['name' => 'Samsung galaxy S9', 'status' => 1 ],
             ['name' => 'Samsung galaxy A50', 'status' => 0 ],
             ['name' => 'Samsung galaxy A50s', 'status' => 1 ],
             ['name' => 'Samsung galaxy S9 Plus', 'status' => 1 ],
            // 10 sản phẩm tiếp theo
            ['name' => 'Samsung galaxy M51', 'status' => 0 ],
             ['name' => 'Samsung galaxy A72', 'status' => 0 ],
             ['name' => 'Samsung galaxy A52', 'status' => 1 ],
             ['name' => 'Samsung galaxy A51', 'status' => 0 ],
             ['name' => 'Samsung galaxy A32', 'status' => 0 ],
             ['name' => 'Samsung galaxy A52 5G', 'status' => 1 ],
             ['name' => 'Samsung galaxy Note 10 ','status' => 1 ],
             ['name' => 'Samsung galaxy Note 10 Plus ', 'status' => 1 ],
             ['name' => 'Samsung galaxy S10 ', 'status' => 0 ],
             ['name' => 'Samsung galaxy S20 ', 'status' => 1 ],

        ]);

    }
}
