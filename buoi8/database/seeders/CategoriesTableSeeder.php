<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
            // 10 Danh Mục
            // ['name' => 'Điện Thoại' ],
             ['name' => 'SamSung', 'parent_id' => 1 ],
             ['name' => 'Iphone', 'parent_id' => 1 ],
             ['name' => 'Oppo', 'parent_id' => 1 ],
             ['name' => 'Xiaomi', 'parent_id' => 1 ],
             ['name' => 'Vivo','parent_id' => 1 ],
             ['name' => 'Laptop'  ],
             ['name' => 'Macbook', 'parent_id' => 7 ],
             ['name' => 'Asus', 'parent_id' => 7 ],
             ['name' => 'dell', 'parent_id' => 7 ]
            
        ]);

    }
}
