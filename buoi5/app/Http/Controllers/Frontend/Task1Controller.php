<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Task;
use Illuminate\Support\Facades\Redirect;

class TaskController extends Controller
{
   public function index( Request $request) {
    $tasks = task::all();
    // foreach($tasks as $task) {
    //   echo $task->name. '<br>';
    // }
     return view('task.index',[
       'tasks'=>$tasks
     ]);
    
   }
   public function store(Request $request)
    {
      $name = $request->get(key: 'name');
      $content = $request->get(key: 'content');
      $deadline = $request->get(key: 'deadline');
        $task = new Task();
        $task->name = $name;
        $task->status = 1;
        $task->deadline = $deadline;
        $task->content = $content;
        $task->save();
        return Redirect()->route(route: 'task.index');
    }

  //  public function show($id) {

  //  $task = Task::find($id);
  //  dd($task);

  //  }

}
