@extends('layout.master') 
@section('content')
   <!-- Current Tasks -->
        <div class="panel panel-default">


            <div class="panel-body">
                <table class="table table-striped task-table">
                    <thead>
                    <th>Tên công việc</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                    @foreach($tasks as $task)
                    <tr>
                        <td class="table-text"><div>{{$task->name}}</div></td>
                        <!-- Task Complete Button -->
                        <td>
                            <a href="{{ url('task/complete/1') }}" type="submit" class="btn btn-success">
                                <i class="fa fa-btn fa-check"></i>Hoàn thành
                            </a>
                        </td>
                        <!-- Task Delete Button -->
                        <td>
                            <form action="{{ url('task/1') }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-btn fa-trash"></i>Xoá
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection