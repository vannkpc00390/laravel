<?php

use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\QueryException;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::group([
        'namespace' => 'Frontend',
        'prefix' => '/'
    ], function (){
        //
        Route::get('/', 'HomeController@index')->name('frontend.home');
    
    });
    
    Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login.form');
    Route::post('admin/login', 'Auth\LoginController@login')->name('login.store');
    Route::get('admin/logout', 'Auth\LogoutController@logout')->name('logout');
    Route::get('admin/register', 'Auth\registerController@showForm')->name('register.form');
    Route::post('admin/register', 'Auth\registerController@register')->name('register.store');

Route::group([
    'namespace' => 'Backend',
    'prefix' => 'admin'
], function (){
    // Trang dashboard - trang chủ admin
    Route::get('/', 'DashboardController@index')
    ->middleware(middleware:'auth')
    ->name('backend.dashboard');
    // Quản lý sản phẩm
    Route::group(['prefix' => 'products'], function(){
       Route::get('/', 'ProductController@index')
       ->middleware(middleware:'auth')
       ->name('backend.product.index');
       Route::get('/create', 'ProductController@create')
       ->middleware(middleware:'auth')
       ->name('backend.product.create');
       Route::post('/store', 'ProductController@store')->name('backend.product.store');
       Route::get('/edit/{id}', 'ProductController@edit')->name('backend.product.edit');
    });
    //Quản lý người dùng
    Route::group(['prefix' => 'users'], function(){
        Route::get('/', 'UserController@index')
        ->middleware(middleware:'auth')
        ->name('backend.user.index');
        Route::get('/create', 'UserController@create')
        ->middleware(middleware:'auth')
        ->name('backend.user.create');
    });
     //Quản lý danh Mục
     Route::group(['prefix' => 'categories'], function(){
        Route::get('/', 'CategoriesController@index')
        ->middleware(middleware:'auth')
        ->name('backend.category.index');
        Route::get('/create', 'CategoriesController@create')
        ->middleware(middleware:'auth')
        ->name('backend.category.create');
        Route::post('/store', 'CategoriesController@store')->name('backend.category.store');
    });
});
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
