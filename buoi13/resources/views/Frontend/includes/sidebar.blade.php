<div class="col-md-3 col-sm-3 col-xs-12 col-left collection-sidebar" id="filter-sidebar">
    <div class="close-sidebar-collection hidden-lg hidden-md">
        <span>filter</span><i class="icon_close ion-close"></i>
    </div>
    <div class="filter filter-cate">
        <ul class="wiget-content v3">
            <li class="active"><a href="#">All Categories</a>
                
                <ul class="wiget-content v4">
                    <li><h3>Cell Phone &amp; Accessories <span class="number">(120)</span></h3></li>
                    <li><a href="#">Unlocked Cell Phones <span class="number">(80)</span></a></li>
                    <li><a href="#">Carrier Phones <span class="number">(39)</span></a></li>
                    <li><a href="#">Accessories  <span class="number">(56)</span></a></li>
                    <li><a href="#">Wearable Technology <span class="number">(10)</span></a></li>
                    <li><a href="#">Cases <span class="number">(30)</span></a></li>
                    <li><a href="#">Bluetooth Headsets <span class="number">(30)</span></a></li>
                    <li><a href="#">Top 20 Staff Pick <span class="number">(30)</span></a></li>
                </ul>
            </li>
            
        </ul>
    </div>
    <div class="filter filter-group">
        <h1 class="widget-blog-title">Product filter</h1>
        <div class="filter-price filter-inside">
            <h3>Price</h3>
            <div class="filter-content">
                <div class="price-range-holder">
                    <div class="slider slider-horizontal" id=""><div class="slider-track"><div class="slider-track-low" style="left: 0px; width: 0%;"></div><div class="slider-selection" style="left: 0%; width: 50%;"></div><div class="slider-track-high" style="right: 0px; width: 50%;"></div></div><div class="tooltip tooltip-main top" role="presentation" style="left: 25%; margin-left: -33px;"><div class="tooltip-arrow"></div><div class="tooltip-inner">100 : 400</div></div><div class="tooltip tooltip-min top" role="presentation" style="left: 0%; margin-left: 0px; display: none;"><div class="tooltip-arrow"></div><div class="tooltip-inner">100</div></div><div class="tooltip tooltip-max top" role="presentation" style="left: 50%; margin-left: 0px; display: none;"><div class="tooltip-arrow"></div><div class="tooltip-inner">400</div></div><div class="slider-handle min-slider-handle round" role="slider" aria-valuemin="100" aria-valuemax="700" aria-valuenow="100" tabindex="0" style="left: 0%;"></div><div class="slider-handle max-slider-handle round" role="slider" aria-valuemin="100" aria-valuemax="700" aria-valuenow="400" tabindex="0" style="left: 50%;"></div></div><input type="text" class="price-slider" value="100,400" data-value="100,400" style="display: none;">
                </div>
                <span class="min-max">
                    Price: $25 — $258
                </span>
                <a href="#" class="btn-filter e-gradient">Filter</a>
            </div>
        </div>
        <div class="filter-brand filter-inside">
            <h3>Brands</h3>
            <ul class="e-filter brand-filter">
                <li><a href="#">Apple <span class="number">(80)</span></a></li>
                <li class="active-filter"><a href="#">Samsung <span class="number">(80)</span></a></li>
                <li><a href="#">LG <span class="number">(80)</span></a></li>
                <li><a href="#">Blackberry <span class="number">(80)</span></a></li>
                <li><a href="#">Oppo <span class="number">(80)</span></a></li>
                <li><a href="#">Panasonic <span class="number">(80)</span></a></li>
            </ul>
            <a href="#" class="btn-showmore">Show more +</a>
        </div>
        <div class="filter-color filter-brand filter-inside">
            <h3>Color</h3>
            <ul class="e-filter brand-filter">
                <li><a href="#">Black <span class="number">(80)</span></a></li>
                <li><a href="#">Black Leather <span class="number">(80)</span></a></li>
                <li class="active-filter"><a href="#">Black with Red <span class="number">(80)</span></a></li>
                <li><a href="#">Gold <span class="number">(80)</span></a></li>
                <li><a href="#">Spacegrey  <span class="number">(80)</span></a></li>
            </ul>
            <a href="#" class="btn-showmore">Show more +</a>
        </div>
    </div>
    <div class="filter filter-product e-category">
        <h1 class="widget-blog-title">Top Products</h1>
        <div class="owl-carousel owl-theme js-owl-post owl-loaded owl-drag">
            

            

            
        <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 690px;"><div class="owl-item active" style="width: 230px;"><div class="item">
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/phone4.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema  </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/sound.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema  </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/ring.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/macbookair.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/phone3.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
            </div></div><div class="owl-item" style="width: 230px;"><div class="item">
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/phone4.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema  </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/sound.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema  </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/ring.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/macbookair.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontendimages/phone3.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
            </div></div><div class="owl-item" style="width: 230px;"><div class="item">
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/phone4.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema  </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/sound.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema  </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="/frontend/images/ring.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="images/macbookair.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>
                    </div>
                </div>
                <div class="cate-item">
                    <div class="product-img">
                        <a href="#"><img src="images/phone3.jpg" alt="" class="img-reponsive"></a>
                    </div>
                    <div class="product-info">
                        <h3 class="product-title"><a href="#">Epson Home Cinema </a></h3>
                        <div class="product-price v2"><span>$780.00</span></div>