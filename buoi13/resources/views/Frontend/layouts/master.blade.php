<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title> @yield('title')</title>
    <link rel="stylesheet" href="/frontend/css/owl.carousel.min.css">
    <link rel="shortcut icon" href="/frontend/images/favicon.png" type="image/png">
    <link rel="stylesheet" href="/frontend/css/slick.css">
    <link rel="stylesheet" href="/frontend/css/slick-theme.css">
    <link rel="stylesheet" href="/frontend/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="/frontend/text/css" href="/frontend/css/style.css">
</head>

<body>
    @include('Frontend.includes.navbar')

    @include('Frontend.includes.sidebar')

    @include('Frontend.includes.footer')

    <script src="/frontend/js/jquery.js"></script>
    <script src="/frontend/js/bootstrap.js"></script>
    <script src="/frontend/js/owl.carousel.min.js"></script>
    <script src="/frontend/js/slick.js"></script>
    <script src="/frontend/js/countdown.js"></script>
    <script src="/frontend/js/main.js"></script>
</body>

</html>