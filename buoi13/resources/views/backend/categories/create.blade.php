@extends('backend.layouts.master')
    @section('title')
        Thêm Mới Danh Mục
    @endsection('title')
    @section('content-header')
   <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Tạo Danh Mục</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Danh Mục</a></li>
                    <li class="breadcrumb-item active">Tạo Danh Mục</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @endsection
    @section('content')
    
    <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tạo Danh Mục</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="{{ route('backend.category.store') }}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Danh Mục</label>
                                <input type="text" class="form-control" name="name"  id="" placeholder="Điền tên Danh Mục ">
                            </div>
                            <div class="form-group">
                                <label>Danh mục Cha</label>
                                <select class="form-control select2" name="parent_id" style="width: 100%;">
                                    <option>--Chọn danh mục---</option>
                                    @foreach ($categories as  $category)
                                        @if ($category->parent_id == null)
                                            <option value="{{$category->id}}">{{$category->name}}</option> 
                                        @endif
                                @endforeach 
                                </select>
                            </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <a href="{{ route('backend.category.index') }}" class="btn btn-default">Huỷ bỏ</a>
                            <button type="submit" class="btn btn-success">Tạo mới</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row main row -->
    </div><!-- /.container-fluid -->
    @endsection