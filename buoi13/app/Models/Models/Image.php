<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
    use HasFactory;
    public function getImageUrlAttribute() {
        return url(\Illuminate\Support\Facades\Storage::url($this->path));
    }
}
