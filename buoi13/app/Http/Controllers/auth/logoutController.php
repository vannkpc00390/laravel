<?php


namespace App\Http\Controllers\auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\Auth;

class logoutController extends Controller{
    public function logout(Request $request) {
        Auth::logout();
        $request ->session()->invalidate();
        $request ->session()->regenerateToken();
        return redirect()->route(route:'login.form');
    }
}