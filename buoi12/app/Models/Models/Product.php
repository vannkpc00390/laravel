<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'Products';
    const STATUS_INTI = 0 ;
    const STATUS_BUY = 1 ;
    const STATUS_STOP = -1 ;

    public static $status_text = [
        self:: STATUS_INTI => 'Đang nhập' ,
        self:: STATUS_BUY => 'Mở bán' ,
        self:: STATUS_STOP => 'Hết hàng' 
    ];
    public function image(){
        return $this->hasMany(image::class);
    }
    public function category(){
        return $this->belongsTo(category::class);
    }
    public function user(){
        return $this->belongsTo(user::class);
    }
    use HasFactory;
}
