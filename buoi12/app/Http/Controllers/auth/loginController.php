<?php


namespace App\Http\Controllers\auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller{
    use Authenticatable;

    public function showLoginForm(){
        return view('backend.auth.login');
    }
    public function login(Request $request){
        $data = $request ->validate([
            'email'=>['required', 'email'],
            'password' =>['required']
        ]);
        if (Auth::attempt($data)) {

            // Authentication passed...
            $request->session()->regenerate();
            return redirect()->intended('/admin');
        }
        else {
            return back()-> withErrors([
                'email' => 'Email Bạn Nhập Không Đúng',
                'password' => "password Bạn Nhập Không Đúng"
            ]);
        }
    }
}