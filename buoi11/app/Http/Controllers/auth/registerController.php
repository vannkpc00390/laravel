<?php


namespace App\Http\Controllers\auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\Auth;
use Laravel\Jetstream\Jetstream;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class registerController extends Controller{
    public function showForm(){
        return view('backend.auth.register');
    }
    public function register(Request $request){
        $input = $request;
        User::create([
            'name' => $request->get(key:'name'),
            'email' => $request->get(key:'email'),
            'password' => Hash::make(  $request->get(key:'password')),
        ]);
        return redirect()->route(route:'login.form');
    }
}